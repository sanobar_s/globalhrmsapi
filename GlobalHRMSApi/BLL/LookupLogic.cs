﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using GlobalHRMSApi.Repositories;
using GlobalHRMSApi.Models;
using System.Data.Entity.Core.Objects;
using System.Web.Http;

namespace GlobalHRMSApi.BLL
{
	public class LookupLogic
	{
		HRMSManagementEntities hrmsEntities = new HRMSManagementEntities();

		public List<Country> GetCountries(int? id)
		{
			List<Country> retCountryList = null;
			ObjectResult<GetCountries_Result> countryList = hrmsEntities.GetCountries(id);
			if (countryList != null)
			{
				retCountryList = countryList.ToList().Select(x => new Country()
				{
					ID = x.ID,
					Name = x.Name,
					IsActive = x.IsActive
				}).ToList();
			}
			return retCountryList;
		}

		public List<State> GetStates(int? countryId, int? id)
		{
			List<State> retStateList = null;
			ObjectResult<GetStates_Result> stateList = hrmsEntities.GetStates(id, countryId);
			if (stateList != null)
			{
				retStateList = stateList.ToList().Select(x => new State()
				{
					ID = x.ID,
					Name = x.Name,
					CountryId = x.CountryId,
					IsActive = x.IsActive
				}).ToList();
			}
			return retStateList;
		}

		public List<City> GetCities(int? stateId, int? id)
		{
			List<City> retCityList = null;
			ObjectResult<GetCities_Result> citiesList = hrmsEntities.GetCities(id, stateId);
			if (citiesList != null)
			{
				retCityList = citiesList.ToList().Select(x => new City()
				{
					ID = x.ID,
					Name = x.Name,
					StateId = x.StateId,
					IsActive = x.IsActive
				}).ToList();
			}
			return retCityList;
		}

		public List<BloodGroup> GetBloodGroups(int? id)
		{
			List<BloodGroup> retBloodGroupList = null;
			ObjectResult<GetBloodGroups_Result> bloodGroupList = hrmsEntities.GetBloodGroups(id);
			if (bloodGroupList != null)
			{
				retBloodGroupList = bloodGroupList.ToList().Select(x => new BloodGroup()
				{
					ID = x.ID,
					Name = x.Name,
					IsActive = x.IsActive
				}).ToList();
			}
			return retBloodGroupList;
		}

		public List<Gender> GetGenders(int? id)
		{
			List<Gender> retGenderList = null;
			ObjectResult<GetGenders_Result> genderList = hrmsEntities.GetGenders(id);
			if (genderList != null)
			{
				retGenderList = genderList.ToList().Select(x => new Gender()
				{
					ID = x.ID,
					Name = x.Name,
					IsActive = x.IsActive
				}).ToList();
			}
			return retGenderList;
		}

		public List<Religion> GetReligions(int? id)
		{
			List<Religion> retReligionList = null;
			ObjectResult<GetReligions_Result> religionList = hrmsEntities.GetReligions(id);
			if (religionList != null)
			{
				retReligionList = religionList.ToList().Select(x => new Religion()
				{
					ID = x.ID,
					Name = x.Name,
					IsActive = x.IsActive
				}).ToList();
			}
			return retReligionList;
		}

        public List<AppointmentType> GetAppointmentTypes(int? id)
        {
            List<AppointmentType> retAppointmentTypeList = null;
            ObjectResult<GetAppointmentTypes_Result> appointmentTypeList = hrmsEntities.GetAppointmentTypes(id);
            if (appointmentTypeList != null)
            {
                retAppointmentTypeList = appointmentTypeList.ToList().Select(x => new AppointmentType()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retAppointmentTypeList;
        }

        public List<Bank> GetBanks(int? id)
        {
            List<Bank> retBankList = null;
            ObjectResult<GetBanks_Result> bankList = hrmsEntities.GetBanks(id);
            if (bankList != null)
            {
                retBankList = bankList.ToList().Select(x => new Bank()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retBankList;
        }

        public List<BankBranch> GetBankBranches(int? bankId, int? id)
        {
            List<BankBranch> retBankBranchList = null;
            ObjectResult<GetBankBranches_Result> bankBranchList = hrmsEntities.GetBankBranches(id, bankId);
            if (bankBranchList != null)
            {
                retBankBranchList = bankBranchList.ToList().Select(x => new BankBranch()
                {
                    ID = x.ID,
                    Name = x.Name,
                    BankId = x.BankId,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retBankBranchList;
        }

        public List<Category> GetCategories(int? id)
        {
            List<Category> retCategoryList = null;
            ObjectResult<GetCategories_Result> categoryList = hrmsEntities.GetCategories(id);
            if (categoryList != null)
            {
                retCategoryList = categoryList.ToList().Select(x => new Category()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retCategoryList;
        }

        public List<Designation> GetDesignations(int? id)
        {
            List<Designation> retDesignationList = null;
            ObjectResult<GetDesignations_Result> designationList = hrmsEntities.GetDesignations(id);
            if (designationList != null)
            {
                retDesignationList = designationList.ToList().Select(x => new Designation()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retDesignationList;
        }

        public List<Education> GetEducations(int? id)
        {
            List<Education> retEducationList = null;
            ObjectResult<GetEducations_Result> educationList = hrmsEntities.GetEducations(id);
            if (educationList != null)
            {
                retEducationList = educationList.ToList().Select(x => new Education()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retEducationList;
        }

        public List<Grade> GetGrades(int? id)
        {
            List<Grade> retGradeList = null;
            ObjectResult<GetGrades_Result> gradeList = hrmsEntities.GetGrades(id);
            if (gradeList != null)
            {
                retGradeList = gradeList.ToList().Select(x => new Grade()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retGradeList;
        }

        public List<Relation> GetRelations(int? id)
        {
            List<Relation> retRelationList = null;
            ObjectResult<GetRelations_Result> relationList = hrmsEntities.GetRelations(id);
            if (relationList != null)
            {
                retRelationList = relationList.ToList().Select(x => new Relation()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retRelationList;
        }

        public List<Unit> GetUnits(int? id)
        {
            List<Unit> retUnitList = null;
            ObjectResult<GetUnits_Result> unitList = hrmsEntities.GetUnits(id);
            if (unitList != null)
            {
                retUnitList = unitList.ToList().Select(x => new Unit()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retUnitList;
        }

        public List<ModeOfPayment> GetModeOfPayments(int? id)
        {
            List<ModeOfPayment> retModeOfPaymentList = null;
            ObjectResult<GetModeOfPayments_Result> modeOfPaymentList = hrmsEntities.GetModeOfPayments(id);
            if (modeOfPaymentList != null)
            {
                retModeOfPaymentList = modeOfPaymentList.ToList().Select(x => new ModeOfPayment()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retModeOfPaymentList;
        }

        public List<MaritalStatus> GetMaritalStatus(int? id)
        {
            List<MaritalStatus> retMaritalStatusList = null;
            ObjectResult<GetMaritalStatus_Result> maritalStatusList = hrmsEntities.GetMaritalStatus(id);
            if (maritalStatusList != null)
            {
                retMaritalStatusList = maritalStatusList.ToList().Select(x => new MaritalStatus()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retMaritalStatusList;
        }

        public List<Department> GETDepartments(int? id)
        {
            List<Department> retDepartmentList = null;
            ObjectResult<GETDepartments_Result> departmentList = hrmsEntities.GETDepartments(id);
            if (departmentList != null)
            {
                retDepartmentList = departmentList.ToList().Select(x => new Department()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retDepartmentList;
        }

        public List<Company> GetCompanies(int? id)
        {
            List<Company> retCompanyList = null;
            ObjectResult<GetCompanies_Result> companyList = hrmsEntities.GetCompanies(id);
            if (companyList != null)
            {
                retCompanyList = companyList.ToList().Select(x => new Company()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retCompanyList;
        }

        public List<Contractor> GetContractors(int? id)
        {
            List<Contractor> retContractorList = null;
            ObjectResult<GetContractors_Result> contractorList = hrmsEntities.GetContractors(id);
            if (contractorList != null)
            {
                retContractorList = contractorList.ToList().Select(x => new Contractor()
                {
                    ID = x.ID,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
            return retContractorList;
        }

        public int InseretAppointment(AppointmentType appointmentType)
        {
            try
            {
                appointmentType.CreatedDateTime = DateTime.Now;
                appointmentType.UpdatedDateTime = DateTime.Now;
                
                DataTable appointmentTypeDataTable = Common.Common.ToDataTable(new List<AppointmentType>() { appointmentType });
                SqlParameter appointmentTypeParams = new SqlParameter("@appointTypeMaster", SqlDbType.Structured)
                {
                    Value = appointmentTypeDataTable,
                    TypeName = "dbo.UDT_AppointmentTypeMaster"
                },
                appointmentId = new SqlParameter("@appointmentId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                }; 

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertAppointmentType @appointTypeMaster, @appointmentId output, @errorMessage output", appointmentTypeParams, appointmentId,errorMessage);

                return Convert.ToInt32(appointmentId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertCategory(Category category)
        {
            try
            {
                DataTable categoryDataTable = Common.Common.ToDataTable(new List<Category>() { category });
                SqlParameter categoryParams = new SqlParameter("@categoryMaster", SqlDbType.Structured)
                {
                    Value = categoryDataTable,
                    TypeName = "dbo.UDT_CategoryMaster"
                },
                categoryId = new SqlParameter("@categoryId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertCategory @categoryMaster, @categoryId output, @errorMessage output", categoryParams, categoryId, errorMessage);

                return Convert.ToInt32(categoryId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertUnit(Unit unit)
        {
            try
            {
                DataTable unitDataTable = Common.Common.ToDataTable(new List<Unit>() { unit });
                SqlParameter unitParams = new SqlParameter("@unitMaster", SqlDbType.Structured)
                {
                    Value = unitDataTable,
                    TypeName = "dbo.UDT_UnitMaster"
                },
                unitId = new SqlParameter("@unitId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertUnit @unitMaster, @unitId output, @errorMessage output", unitParams, unitId, errorMessage);

                return Convert.ToInt32(unitId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertGrade(Grade grade)
        {
            try
            {
                DataTable gradeDataTable = Common.Common.ToDataTable(new List<Grade>() { grade });
                SqlParameter gradeParams = new SqlParameter("@gradeMaster", SqlDbType.Structured)
                {
                    Value = gradeDataTable,
                    TypeName = "dbo.UDT_GradeMaster"
                },
                gradeId = new SqlParameter("@gradeId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertGrade @gradeMaster, @gradeId output, @errorMessage output", gradeParams, gradeId, errorMessage);

                return Convert.ToInt32(gradeId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertReligion(Religion religion)
        {
            try
            {
                DataTable religionDataTable = Common.Common.ToDataTable(new List<Religion>() { religion });
                SqlParameter religionParams = new SqlParameter("@religionMaster", SqlDbType.Structured)
                {
                    Value = religionDataTable,
                    TypeName = "dbo.UDT_ReligionMaster"
                },
                religionId = new SqlParameter("@religionId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertReligion @religionMaster, @religionId output, @errorMessage output", religionParams, religionId, errorMessage);

                return Convert.ToInt32(religionId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertEducation(Education education)
        {
            try
            {
                DataTable educationDataTable = Common.Common.ToDataTable(new List<Education>() { education });
                SqlParameter educationParams = new SqlParameter("@educationMaster", SqlDbType.Structured)
                {
                    Value = educationDataTable,
                    TypeName = "dbo.UDT_EducationMaster"
                },
                educationId = new SqlParameter("@educationId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertEducation @educationMaster, @educationId output, @errorMessage output", educationParams, educationId, errorMessage);

                return Convert.ToInt32(educationId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertBank(Bank bank)
        {
            try
            {
                DataTable bankDataTable = Common.Common.ToDataTable(new List<Bank>() { bank });
                SqlParameter bankParams = new SqlParameter("@bankMaster", SqlDbType.Structured)
                {
                    Value = bankDataTable,
                    TypeName = "dbo.UDT_BankMaster"
                },
                bankId = new SqlParameter("@bankId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertBank @bankMaster, @bankId output, @errorMessage output", bankParams, bankId, errorMessage);

                return Convert.ToInt32(bankId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertCountry(Country country)
        {
            try
            {
                DataTable countryDataTable = Common.Common.ToDataTable(new List<Country>() { country });
                SqlParameter countryParams = new SqlParameter("@countryMaster", SqlDbType.Structured)
                {
                    Value = countryDataTable,
                    TypeName = "dbo.UDT_CountryMaster"
                },
                countryId = new SqlParameter("@countryId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertCountry @countryMaster, @countryId output, @errorMessage output", countryParams, countryId, errorMessage);

                return Convert.ToInt32(countryId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertState(State state)
        {
            try
            {
                DataTable stateDataTable = Common.Common.ToDataTable(new List<State>() { state });
                SqlParameter stateParams = new SqlParameter("@stateMaster", SqlDbType.Structured)
                {
                    Value = stateDataTable,
                    TypeName = "dbo.UDT_StateMaster"
                },
                stateId = new SqlParameter("@stateId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertState @stateMaster, @stateId output, @errorMessage output", stateParams, stateId, errorMessage);

                return Convert.ToInt32(stateId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }

        public int InsertCity(City city)
        {
            try
            {
                DataTable cityDataTable = Common.Common.ToDataTable(new List<City>() { city });
                SqlParameter cityParams = new SqlParameter("@cityMaster", SqlDbType.Structured)
                {
                    Value = cityDataTable,
                    TypeName = "dbo.UDT_CityMaster"
                },
                cityId = new SqlParameter("@cityId", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                },
                errorMessage = new SqlParameter()
                {
                    ParameterName = "@errorMessage",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 500,
                    Direction = ParameterDirection.Output
                };

                hrmsEntities.Database.ExecuteSqlCommand("exec dbo.InsertCity @cityMaster, @cityId output, @errorMessage output", cityParams, cityId, errorMessage);

                return Convert.ToInt32(cityId.Value);
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex.Message.ToString());
            }
        }
    }
}